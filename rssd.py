#!/usr/bin/env python
# -*- coding: utf-8 -*-

import diaspy
import feedparser
import os

#Loga na Diaspora
c = diaspy.connection.Connection(pod='', username='', password='')
c.login()

#Cria log
open('rssdtt.log', 'a').close()

#Parseia o feed
item = feedparser.parse('http://seu.feed')
title = item['items'][0]['title']
link = item['items'][0]['link']

#Lê o log
log = open("rssdtt.log", "r").readline()

stream = diaspy.streams.Stream(c)

#Compara o último link postado com o log e publica
if link != log:
    stream.post(title + ' ' + link)

#Grava o último link postado no log    
log = open('rssdtt.log', 'w')
log.write(link)
log.close()